package com.soyoger.leetcode.utils;

import java.text.SimpleDateFormat;

/**
 * @program: leet-code
 * @description:
 * @packagename:
 * @author: yongjie.su
 * @date: 2021-04-13 21:10
 **/
public class TimeHelper {
    public static void main(String[] args) {
        String date = "1438692801766";
        long times = Long.parseLong(date);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(times);
        System.out.println(dateString);
    }
}
