package com.soyoger.leetcode.data_structure.tree;

/**
 * @program: leet-code
 * @description:
 * @packagename:
 * @author: yongjie.su
 * @date: 2021-04-22 08:20
 **/
public class BinaryTreeNode {
    int val;
    BinaryTreeNode left;
    BinaryTreeNode right;

    public BinaryTreeNode() {
    }

    public BinaryTreeNode(int val) {
        this.val = val;
    }

    public BinaryTreeNode(int val, BinaryTreeNode left, BinaryTreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}
