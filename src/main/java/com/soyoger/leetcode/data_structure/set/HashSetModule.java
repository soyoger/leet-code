package com.soyoger.leetcode.data_structure.set;

import java.util.HashSet;
import java.util.Set;

/**
 * @program: leet-code
 * @description:
 * @packagename:
 * @author: yongjie.su
 * @date: 2021-04-21 20:45
 **/
public class HashSetModule {
    public static void main(String[] args) {
        //新建集合
        Set<Integer> set = new HashSet<Integer>();
        //添加元素
        set.add(1);
        set.add(2);
        set.add(3);
        //删除元素
        set.remove(2);
        //查询
        set.contains(1);
        //长度
        set.size();
        //遍历
        set.forEach(x ->{
            System.out.println(x);
        });
        for(Integer x:set){
            System.out.println(x);
        }
    }
}
