package com.soyoger.leetcode.data_structure.stack;

import java.util.Stack;

/**
 * @program: leet-code
 * @description:
 * @packagename:
 * @author: yongjie.su
 * @date: 2021-04-14 18:45
 **/
public class StackModuleExe {
    public static void main(String[] args) {
        //创建
        Stack stack = new Stack();
        //添加
        stack.push(1);
        stack.push(2);
        stack.push(3);
        //访问
        stack.peek();
        //出栈
        stack.pop();
        //长度
        stack.size();
        //遍历
        while (!stack.isEmpty()) {
            Object pop = stack.pop();
            System.out.println(pop.toString());
        }
    }

    public boolean isValid(String s) {
        if (s == null || s.length() == 0) return true;
        Stack stack = new Stack();
        char[] arr = s.toCharArray();
        for (char ch : arr) {
            if (ch == '(' || ch == '{' || ch == '[') {
                stack.push(ch);
            } else if (ch == ')') {
                if (stack.size() > 0) {
                    char tmp = (char) stack.pop();
                    if (tmp != '(') {
                        return false;
                    }
                } else {
                    return false;
                }
            } else if (ch == '}') {
                if (stack.size() > 0) {
                    char tmp = (char) stack.pop();
                    if (tmp != '{') {
                        return false;
                    }
                } else {
                    return false;
                }
            } else if (ch == ']') {
                if (stack.size() > 0) {
                    char tmp = (char) stack.pop();
                    if (tmp != '[') {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }

    public boolean isValid2(String s) {
        if (s == null || s.length() == 0) return true;
        Stack stack = new Stack();
        char[] arr = s.toCharArray();
        for (char ch : arr) {
            if (ch == '('){ stack.push(')');}
            if (ch == '{'){stack.push('}');}
            if(ch == '[') {stack.push('}');}
            if(stack.size()> 0){
                char tmp = (char) stack.pop();
                if(ch != tmp){
                    return false;
                }
            }else{
                return false;
            }
        }
        return stack.isEmpty();
    }

    public boolean isValid3(String s) {
        if (s == null || s.length() == 0) return true;
        for(int i=0;i<s.length()/2;i++){
            s.replace("()","").replace("{}","").replace("[]","");
        }
        return  s.length()>0;
    }

}
