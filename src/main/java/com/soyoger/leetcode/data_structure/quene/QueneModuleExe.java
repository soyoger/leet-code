package com.soyoger.leetcode.data_structure.quene;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @program: leet-code
 * @description:
 * @packagename:
 * @author: yongjie.su
 * @date: 2021-04-14 17:55
 **/
public class QueneModuleExe {

    public static void main(String[] args) {
        //创建队列
        Queue<Integer> queue = new LinkedList<>();
        //添加元素
        queue.add(1);
        queue.add(2);
        queue.add(3);
        //获取元素
        int a = queue.peek();
        System.out.println(a);
        //删除
        //queue.poll();
        //queue.remove();
        //长度
        int size = queue.size();
        //遍历
        while (!queue.isEmpty()) {
            int tmp = queue.remove();
            System.out.println(tmp);
            //break;
        }

    }
}
