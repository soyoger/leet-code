package com.soyoger.leetcode.data_structure.hashmap;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: leet-code
 * @description:
 * @packagename:
 * @author: yongjie.su
 * @date: 2021-04-15 09:50
 **/
public class HashMapModule {
    public static void main(String[] args) {
        //创建HashMap
        Map<String, Integer> map = new HashMap<String, Integer>();
        //添加元素
        map.put("a", 1);
        map.put("b", 2);
        map.put("c", 3);
        //访问元素
        map.get("a");
        //删除元素
        map.remove("a");
        //修改元素
        map.replace("b", 22);
        //检查key是否存在
        map.containsKey("a");
        //长度
        map.size();

        map.isEmpty();
        //遍历元素
        for (String key : map.keySet()) {
            Integer v = map.get(key);
            System.out.println(v);
        }
        map.forEach((k, v) ->
        {
            System.out.println(v);
        });

        map.keySet();
        map.values();
    }
}
