package com.soyoger.leetcode.data_structure.linked;

import java.util.Stack;

/**
 * @program: leet-code
 * @description:
 * @packagename:
 * @author: yongjie.su
 * @date: 2021-04-07 22:41
 **/
public class  ReversePrint {

    public static void main(String[] args) {
        ListNode ln1 = new ListNode(1);
        ReversePrint.reversePrint(ln1);
    }

    public static  int[] reversePrint(ListNode head) {
        if(head == null) return new int [0];
        Stack stack = new Stack();
        while(head != null){
            stack.push(head.val);
            head = head.next;
        }
        //这儿长度提前计算保存在len里面
        int len = stack.size();
        int [] arr = new int[len];
        for (int j=0;j<len;j++){
            arr[j] = Integer.valueOf(stack.pop().toString());
        }
        //System.out.println(arr[0]);
        return arr;

    }


}

class ListNode{
    int val;
    ListNode next;
    ListNode(int x) { val = x; }
}
