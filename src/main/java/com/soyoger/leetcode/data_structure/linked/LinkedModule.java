package com.soyoger.leetcode.data_structure.linked;

import java.util.LinkedList;

/**
 * @program: leet-code
 * @description:
 * @packagename:
 * @author: yongjie.su
 * @date: 2021-04-13 21:08
 **/
public class LinkedModule {
    public static void main(String[] args) {
        //创建
        LinkedList<String> linkedList = new LinkedList<String>();

        //添加
        linkedList.add("a");
        linkedList.add("b");
        linkedList.add(2, "c");
        linkedList.addAll(linkedList);

        //搜索元素,返回索引
        int idx = linkedList.indexOf("a");
        //判断元素是否存在，返回布尔值
        boolean flag = linkedList.contains("a");
        //访问
        String s = linkedList.get(0);

        //更新
        linkedList.set(2, "aa");

        //删除
        linkedList.remove("a");

        //长度
        int size = linkedList.size();
    }
}
