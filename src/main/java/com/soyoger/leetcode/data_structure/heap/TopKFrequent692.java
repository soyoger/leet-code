package com.soyoger.leetcode.data_structure.heap;


import java.util.*;

/**
 * @program: leet-code
 * @description:
 * @packagename:
 * @author: yongjie.su
 * @date: 2021-04-22 09:39
 **/
public class TopKFrequent692 {
    public List<String> topKFrequent(String[] words, int k) {
        List<String> list = new ArrayList<>();
        Map<String, Integer> map = new HashMap<>();
        for (String key : words) {
            if (!map.containsKey(key)) {
                map.put(key, 1);
            } else {
                map.replace(key, map.get(key) + 1);
            }
        }

        //大堆
        PriorityQueue<String> queue = new PriorityQueue<>((o1, o2) -> {
            Integer o1Count = map.get(o1);
            Integer o2Count = map.get(o2);
            if (o1Count.equals(o2Count)) {
                return o1.compareTo(o2);
            } else {
                return o2Count - o1Count;
            }
        });

        for (String s : map.keySet()) {
            queue.add(s);
        }

        while (k > 0) {
            list.add(queue.poll());
            k--;
        }
        return list;
    }

}
