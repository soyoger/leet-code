package com.soyoger.leetcode.data_structure.heap;

import java.util.Arrays;
import java.util.Collections;
import java.util.PriorityQueue;

/**
 * @program: leet-code
 * @description:
 * @packagename:
 * @author: yongjie.su
 * @date: 2021-04-22 09:06
 **/
public class FindKthLargest215 {
    public int findKthLargest(int[] nums, int k) {
        Arrays.sort(nums);
        int len = nums.length;
        if (len == 0) return 0;
        //这儿注意定义最大堆，数据堆化，时间复杂度O(n)
        PriorityQueue<Integer> heap = new PriorityQueue(Collections.reverseOrder());
        for (int x : nums) {
            heap.add(x);
        }
        while (k > 1) {
            heap.poll();
            k--;
        }
        return heap.peek();
    }
    public int findKthLargest2(int[] nums, int k) {
        Arrays.sort(nums);
        return nums[nums.length-k];
    }

    public int numRescueBoats(int[] people, int limit) {
        if(people.length == 0) return 0;
        int count = 0;
        Arrays.sort(people);
        int i=0,j=people.length -1;
        while( i!=j){
            if((people[i] + people[j]) == limit){
                count++;
                i++;
                j--;
            }else{
                j--;
            }
        }
        return people.length - count;
    }
}
