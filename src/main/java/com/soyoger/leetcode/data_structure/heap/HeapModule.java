package com.soyoger.leetcode.data_structure.heap;


import java.util.Collections;
import java.util.PriorityQueue;

/**
 * @program: leet-code
 * @description:堆的数据结构 ，其实就是完全二叉树，堆分为最大堆和最小堆
 * @packagename:
 * @author: yongjie.su
 * @date: 2021-04-21 22:15
 **/
public class HeapModule {
    public static void main(String[] args) {
        //创建最小堆
        PriorityQueue<Integer> minHeap = new PriorityQueue();
        //创建最大堆
        PriorityQueue maxHeap = new PriorityQueue(Collections.reverseOrder());
        //添加数据
        minHeap.add(1);
        minHeap.add(2);
        minHeap.add(3);
        minHeap.add(4);
        System.out.println(minHeap.toString());

        maxHeap.add(5);
        maxHeap.add(6);
        maxHeap.add(7);
        maxHeap.add(8);
        System.out.println(maxHeap.toString());
        //访问元素
        minHeap.peek();
        maxHeap.peek();
        //删除元素
        minHeap.poll();
        //长度
        minHeap.size();
    }
}
