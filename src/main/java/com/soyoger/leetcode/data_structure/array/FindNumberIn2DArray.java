package com.soyoger.leetcode.data_structure.array;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @program: leet-code
 * @description:
 * @packagename:
 * @author: yongjie.su
 * @date: 2021-04-08 16:06
 **/
public class FindNumberIn2DArray {

    public static void main(String[] args) {
        int[][] matrix = {{1, 4, 7, 11, 15}, {2, 5, 8, 12, 19}, {3, 6, 9, 16, 22}};
        boolean flag = FindNumberIn2DArray.findNumberIn2DArray(matrix, 20);
        System.out.println(flag);
        int [] arr = {1, 2, 3, 2, 2, 2, 5, 4, 2};
        int res = FindNumberIn2DArray.majorityElement(arr);
        System.out.println(9/2);
    }

    public int[] exchange(int[] a) {
        int i = 0;
        int j = a.length - 1;
        while (i < j) {
            if (a[i] % 2 == 0 && a[j] % 2 == 1) {
                int tmp = a[i];
                a[i] = a[j];
                a[j] = tmp;
                i += 1;
                j -= 1;
            }
            if (a[i] % 2 == 1) {
                --j;
            }
            if (a[j] % 2 == 0) {
                ++i;
            }
        }
        return a;
    }

        public static boolean findNumberIn2DArray ( int[][] matrix, int target){
            if (matrix.length == 0) return false;
            for (int i = 0; i < matrix.length; i++) {
                int[] arr = matrix[i];
                for (int j = 0; j < arr.length; j++) {
                    int temp = arr[j];
                    if (temp == target) {
                        //Integer.MIN_VALUE;
                        return true;
                    }
                    if (temp > target) {
                        break;
                    }
                }
            }
            return false;

        }

    public static int majorityElement(int[] nums) {
        if(nums.length ==0 ) return 0;
        if(nums.length ==1) return nums[0];
        Map<Integer,Integer> map = new HashMap<Integer,Integer>();
        for(int i=0;i<nums.length;i++){
            Integer key = nums[i];
            if(map.containsKey(key)){
                Integer num = map.get(key);
                map.replace(key,num +1);
                if(num>=nums.length/2){
                    return nums[i];
                }
            }else{
                map.put(nums[i],1);
            }
        }
        return 0;
    }

    public char firstUniqChar(String s) {
        if(s.length() == 0) return ' ';
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        char [] arr = s.toCharArray();
        for(char ch:arr){
            if(linkedHashMap.containsKey(ch)){
                linkedHashMap.replace(ch,Integer.valueOf(linkedHashMap.get(ch).toString())+1);
            }else{
                linkedHashMap.put(ch,1);
            }
        }
        linkedHashMap.forEach((key,value)->{
            System.out.println(key);

        });
        for(Object key:linkedHashMap.keySet()){
            Integer v = (Integer) linkedHashMap.get(key);
            if(v ==1){
                return key.toString().toCharArray()[0];
            }
        }
        return ' ';
    }

    public int maxSubArray(int[] nums) {
        if(nums.length == 0) return 0;
        if(nums.length == 1) return 1;
        int start = 0;
        int end = 0;
        int max = Integer.MIN_VALUE;
        for(int i = 0;i<nums.length;i++){

        }
        return 0;


    }

    }
