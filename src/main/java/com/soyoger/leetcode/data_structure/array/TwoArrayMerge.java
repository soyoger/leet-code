package com.soyoger.leetcode.data_structure.array;

import java.util.*;

/**
 * @program: test
 * @description:
 * @packagename: com.soyoger.study.model
 * @author: yongjie.su
 * @date: 2021-04-01 13:19
 **/
public class TwoArrayMerge {
    public static void main(String[] args) {
        int[] arr1 = {1, 2, 5, 6, 9, 0, 0, 0, 0, 0};
        int[] arr2 = {3, 4, 7, 8, 10};
        //TwoArrayMerge.merge(arr1,5,arr2,5);
        int[] arr3 = {0, 0, 0, 0, 0};
        //TwoArrayMerge.merge(arr3,0,arr2,5);
        //System.out.println(Arrays.toString(TwoArrayMerge.twoSum(arr2, 7)));
        System.out.println(Arrays.toString(TwoArrayMerge.twoSum2(arr2, 7)));

    }

    public static int[] twoSum(int[] nums, int target) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length - 1; j++) {
                if (nums[i] + nums[j] == target) {
                    int[] arr = {i, j};
                    return arr;
                }
            }
        }
        return null;
    }

    public static int[] twoSum2(int[] nums, int target) {
        Map<Integer,Integer> map = new HashMap();
        for (int i = 0; i < nums.length; i++) {
            int k = target  - nums[i];
            if(map.containsKey(k)){
                return new int [] {map.get(k),i};
            }
            map.put(nums[i],i);
        }
        return null;
    }


    public static void merge(int[] arr1, int m, int[] arr2, int n) {
        int i = m - 1;
        int j = n - 1;
        int k = m + n - 1;
        while (i >= 0 && j >= 0) {
            if (arr1[i] > arr2[j]) {
                arr1[k--] = arr1[i--];
            } else {
                arr1[k--] = arr2[j--];
            }
        }
        while (j >= 0) {
            arr1[k--] = arr2[j--];

        }
        System.out.println(Arrays.toString(arr1));
    }
}
