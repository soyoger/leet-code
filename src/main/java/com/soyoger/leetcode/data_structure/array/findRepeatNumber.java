package com.soyoger.leetcode.data_structure.array;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: leet-code
 * @description:
 * @packagename:
 * @author: yongjie.su
 * @date: 2021-04-07 22:25
 **/
public class findRepeatNumber {
    public static void main(String[] args) {

    }

    public static int findRepeatNumber(int[] nums) {
        if (nums.length == 0) return 0;
        Map map = new HashMap();
        for (int i = 0; i < nums.length; i++) {
            int key = nums[i];
            if (!map.containsKey(key)) {
                map.put(key, 1);
            } else {
                return key;
            }
        }
        return 0;
    }


}