package com.soyoger.leetcode.data_structure.array;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @program: leet-code
 * @description:
 * @packagename:
 * @author: yongjie.su
 * @date: 2021-04-20 16:52
 **/
public class ContainsDuplicate217 {
    public boolean containsDuplicate(int[] nums) {
        int len = nums.length;
        if (len == 0) return false;
        Map<Integer,Integer> map = new HashMap<> ();
        for (Integer x : nums) {
            if (!map.containsKey(x)) {
                map.put(x, 1);
            } else {
                return true;
            }
        }
        return false;
    }

    public boolean containsDuplicate2(int[] nums) {
        return Arrays.stream(nums).distinct().count() != nums.length;
    }
}
