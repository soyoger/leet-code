package com.soyoger.leetcode.data_structure.array;

/**
 * @program: leet-code
 * @description:
 * @packagename:
 * @author: yongjie.su
 * @date: 2021-04-13 12:12
 **/
public class Fac extends Thread {
    public static void main(String[] args) {

        //测试
        int[] a = {1, 1, 1, 1, 1};
        System.out.println(Fac.findMaxConsecutiveOnes(a));

        System.out.println(Fac.fac(5));
        System.out.println(Fac.fac2(5));
        System.out.println(Fac.fac3(5));


    }

    public static int findMaxConsecutiveOnes(int[] nums) {
        int len = nums.length;
        if (len == 0) return 0;
        int max = Integer.MIN_VALUE, result = 0;
        for (int i = 0; i < len; i++) {
            if (nums[i] == 1) {
                result += nums[i];
            } else {
                max = result > max ? result : max;
                result = 0;
            }
        }
        return result > max ? result : max;
    }

    public static int fac(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        int i = 2;
        int res = 1;
        while (i <= n) {
            res = i * res;
            i++;
        }
        return res;
    }

    public static int fac2(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        int[] arr = new int[n + 1];
        arr[0] = 0;
        arr[1] = 1;
        int i = 2;
        while (i <= n) {
            arr[i] = i * arr[i - 1];
            i++;
        }
        return arr[i - 1];
    }

    public static int fac3(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        int i = 2;
        int res = 0;
        while (i <= n) {
            res = i * fac(i - 1);
            i++;
        }
        return res;
    }

    @Override
    public void run() {

    }
}
