package com.soyoger.leetcode.data_structure.array;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: leet-code
 * @description:
 * @packagename:
 * @author: yongjie.su
 * @date: 2021-04-20 17:04
 **/
public class FindTheDifference389 {

    public char findTheDifference(String s, String t) {
        Map map = new HashMap();
        for(char ch:s.toCharArray()){
            map.put(ch,ch);
        }
        for(char chr:t.toCharArray()){
            if(!map.containsKey(chr)){
                return chr;
            }
        }
        return ' ';
    }
    public static void main(String[] args) {
        System.out.println('b' - 'a');
    }
}
