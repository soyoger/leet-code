package com.soyoger.leetcode.data_structure.array;

import java.util.*;

public class FibonacciModel {
    public static void main(String[] args) {
        FibonacciModel app = new FibonacciModel();
        int res = app.Fibonacci(10);
        System.out.println(res);
        int res2 = app.Fibonacci2(10);
        System.out.println(res2);

    }

    public int Fibonacci(int n) {
        //0 1 1 2 3 5 8 13
        if (n == 0) return 0;
        if (n == 1) return 1;
        return Fibonacci(n - 1) + Fibonacci(n - 2);
    }

    public int Fibonacci2(int n) {
        //0 1 1 2 3 5 8 13
        List<Integer> list = new ArrayList();
        if (n == 0) return 0;
        if (n == 1) return 1;
        list.add(0);
        list.add(1);
        for(int i=2;i<=n;i++){
            Integer x = list.get(i-2);
            Integer y = list.get(i-1);
            list.add(i,(x+y));
        }
        return list.get(n);
    }
}
