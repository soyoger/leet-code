package com.soyoger.leetcode.data_structure.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @program: leet-code
 * @description:
 * @packagename:
 * @author: yongjie.su
 * @date: 2021-04-13 21:07
 **/
public class ArrayModule {
    public static void main(String[] args) {
        //创建数组
        int [] arr = {1,2,3};
        int [] arr1 = new int[] {1,2,3};
        int [] arr2 = new int[3];
        List<Integer> list = new ArrayList<Integer>();

        // 添加元素
        // arr arr1 arr2 添加需要赋值或者重新复制新数组
        list.add(0);
        list.add(1,1);
        list.add(2,2);

        //访问元素
        int temp = arr[0];
        int tmp = list.get(0);

        //修改
        arr[1] = 11;
        list.set(1,11);

        //删除 用list
        list.remove(new Integer(3));
        //list.remove(1); //删除元素
        //list.remove(0); //按索引删除

        //遍历 for循环
        for(int i=0;i<arr.length;i++){
            System.out.println(arr[i]);
        }
        for(int j=0;j>list.size();j++){
            System.out.println(list.get(j));
        }
        for(Integer res:list){
            System.out.println(res);
        }

        list.forEach((s)-> System.out.println(s));

        //长度
        int len = arr.length;
        //int size = list.size();

        //内置排序
        Arrays.sort(arr);
        Collections.sort(list); //升序
        Collections.sort(list,Collections.reverseOrder()); // 降序
    }
}
