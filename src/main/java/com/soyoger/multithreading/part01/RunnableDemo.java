package com.soyoger.multithreading.part01;

/**
 * @program: leet-code
 * @description:
 * @packagename:
 * @author: yongjie.su
 * @date: 2021-04-20 18:19
 **/
public class RunnableDemo implements Runnable{
    @Override
    public void run() {
        System.out.println("Runnable starting");

    }

    public static void main(String[] args) {
        new Thread(new RunnableDemo()).start();
    }
}
