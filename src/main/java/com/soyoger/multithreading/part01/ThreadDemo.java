package com.soyoger.multithreading.part01;

/**
 * @program: leet-code
 * @description:
 * @packagename:
 * @author: yongjie.su
 * @date: 2021-04-20 18:05
 **/
public class ThreadDemo extends Thread {

    @Override
    public void run() {
        super.run();
    }

    public static void main(String[] args) {
        Thread thread1 = new Thread() {
            @Override
            public void run() {
                for (int i = 1; i < 10; i++) {
                    System.out.println("i====" + i);
                }
            }
        };

        Thread thread2 = new Thread() {
            @Override
            public void run() {
                for (int j = 1; j < 10; j++) {
                    System.out.println("j====" + j);
                }
            }
        };
        thread2.setPriority(9);
        thread1.setPriority(1);
        thread1.start();
        thread2.start();


    }

}
