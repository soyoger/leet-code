package com.soyoger.collection_stream.part01;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * @program: leet-code
 * @description:
 * @packagename:
 * @author: yongjie.su
 * @date: 2021-05-16 23:21
 **/
public class ArrayDemo01 {
    public static void main(String[] args) {
        int [] arr = {1,2,3};
        int [] arr2 = Arrays.stream(arr)
                .map(x->x+1)
                .filter(x->x%2==0)
                .toArray();
        System.out.println(Arrays.toString(arr2));
    }
}
