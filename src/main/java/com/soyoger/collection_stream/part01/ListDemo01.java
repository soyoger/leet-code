package com.soyoger.collection_stream.part01;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @program: leet-code
 * @description:
 * @packagename:
 * @author: yongjie.su
 * @date: 2021-05-16 23:35
 **/
public class ListDemo01 {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        Collections.addAll(list, 1, 2, 3, 4, 5, 6, 7, 8);
        Stream<Integer> stream = list.stream();
        //parallelStream 并行，效率更高
        Stream<Integer> intStream = list.parallelStream();
        List<Integer> collect = intStream.distinct().collect(Collectors.toList());
        System.out.println(collect);
        System.out.println("****************");

        Map<Integer, Integer> collect1 = list.stream()
                .distinct()
                .collect(Collectors.toMap(x -> x, x -> x * x));
        System.out.println(collect1);

        long count = list.stream().count();
        System.out.println(count);

        list.parallelStream().forEach(System.out::println);

        //max min
        Integer max = list.stream().max(Integer::compareTo).get();
        System.out.println(max);

        Integer min = list.stream().min(Integer::compareTo).get();
        System.out.println(min);

        //Match
        boolean b = list.stream().allMatch(elem -> elem > 5);
        System.out.println(b);

        boolean b1 = list.stream().anyMatch(elem -> elem > 7);
        System.out.println(b1);

        boolean b2 = list.stream().noneMatch(elem -> elem % 2 == 1);
        System.out.println(b2);

        //findFirst findAny 获取开头的元素 区别多线程环境
        Integer first = list.stream().findFirst().get();
        System.out.println(first);

        //最终操作注意事项，注意流已经关闭

        /**
         * 中间操作，返回Stream对象
         */
        //filter
        Stream<Integer> res1 = list.stream()
                .filter(elem -> elem % 2 == 0);

        //distinct 去重规则与hashset一样
        long count1 = list.stream().distinct().count();

        System.out.println("---------------");
        //sorted
        list.stream().sorted().forEach(System.out::println);
        list.stream().sorted(Comparator.reverseOrder()).forEach(System.out::println);
        list.stream().sorted((e1, e2) -> e1.compareTo(e2)).forEach(System.out::println);

        //skip limit  分页
        list.stream().skip(3).forEach(System.out::println);
        System.out.println("============");
        //map flatMap
        list.parallelStream().map(elem -> elem * 3).forEach(System.out::println);

        String[] str = {"abc", "my"};


    }
}
