package com.soyoger.alitianchi.part01;

/**
 * @program: leet-code
 * @description:
 * @packagename:
 * @author: yongjie.su
 * @date: 2021-04-21 15:35
 **/
public class AddDigits {
    public static int addDigits(int num) {
        if(num%10 == num) return num;
        int sum = 0;
        while(num != 0){
            sum += num%10;
            num = num/10;
        }
        return addDigits(sum);
    }
    public static void main(String[] args) {
        int res = addDigits(38);
        System.out.println(res);
    }
}
