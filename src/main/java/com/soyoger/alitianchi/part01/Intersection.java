package com.soyoger.alitianchi.part01;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: leet-code
 * @description:
 * @packagename:
 * @author: yongjie.su
 * @date: 2021-04-21 15:44
 **/
public class Intersection {

    public static int[] intersection(int[] nums1, int[] nums2) {

        //if(nums1.length ==0 || nums2.length ==0) return arr;
        //int [] tmp1 = Arrays.stream(nums1).distinct().toArray();
        //int [] tmp2 = Arrays.stream(nums2).distinct().toArray();
        List<Integer> set1 = new ArrayList();
        List<Integer> set2 = new ArrayList();
        for(int x:nums1){
            set1.add(x);
        }
        for(int y:nums2){
            set2.add(y);
        }
        set1.retainAll(set2);
        int [] arr = new int[set1.size()];
        int i = 0;
        for(Integer z:set1){
            arr[i++] = z;
        }
        return arr;
    }

    public static void main(String[] args) {
        int[] nums1 = {1,2,3,4};
        int[] nums2 = {1,2,3,3,3};
        int [] res = intersection(nums1,nums2);
        for(int x:res){
            System.out.println(x);
        }

    }
}
